import os, os.path, sys, base64, time
import sqlite3
import AddressRevealingXMLRPCServer
import FlagAuthority

    
class FlagServer(FlagAuthority.FlagAuthority):
    '''an xmlrpc server for creating signed flags'''
    # new requests get added
    # all requests get tracked 
    # response contains:
    #    state: pending, rejected, or signed
    #    public key
    #    signature, if applicable
    def __init__(self,keysize=2048,digestalgo='sha1'):
       self.dbfilename = os.path.join('db','flag.db')
       self.conn = None    # a sqlite3 db connection
       self._dbconnect()
       FlagAuthority.FlagAuthority.__init__(self,digestalgo=digestalgo,keysize=keysize)

    def _dbconnect(self):
        '''check for db; create if missing'''
        sys.stderr.write("DEBUG: dbfilename is '%s'\n" % self.dbfilename)
        if os.path.exists(self.dbfilename):
            self.conn = sqlite3.connect(self.dbfilename)
            return
        #doesn't exist, so create 
        self.conn = sqlite3.connect(self.dbfilename)
        c = self.conn.cursor()

        # unique request and client details
        c.execute('''CREATE TABLE request
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
             uuid TEXT NOT NULL,
             state TEXT NOT NULL,
             type TEXT NOT NULL,
             ipv4 TEXT NOT NULL, socketipv4 TEXT NOT NULL,
             note TEXT)''') #yes, ipv4 as TEXT, sue me :-P

        # signed flags, references requests
        c.execute('''CREATE TABLE flag 
            (requestid INT,
             content TEXT,
             FOREIGN KEY(requestid) REFERENCES request(id) )''')

        # log time of request and its response
        c.execute('''CREATE TABLE polling  
             (timestamp TEXT, response TEXT, ipv4 TEXT, socketipv4 TEXT,
              uuid TEXT)''')

        # hold public and private key, possibly other persistent values
        c.execute('''CREATE TABLE state 
            (name TEXT KEY,
             content TEXT)''')
        self.conn.commit()
        c.close()

def StartXMLRPCFlagServer():
    xmlrpcsrv = AddressRevealingXMLRPCServer.AddressRevealingXMLRPCServer( ("", 8000))

    fs = FlagServer()
    def requestflagb64(localclientaddress, uuid, remoteclientipaddress, type, note):

        """Returns tuple of (requeststatus, base64flagdata). If status is
           'signed', the base64flagdata contains base64 encoded flag.

           Status values: 'pending', 'signed', 'revoked', 'error'"""


        NL = "\r\n"

        #acceptable flag types, more may be added later
        if type.lower() not in ('root', 'user'):
            return ('refused', base64.b64encode("Flag type not supported" + NL))
     
        # query db, if flag is saved in db, return it
        c = fs.conn.cursor()
        c.execute('''SELECT id,state FROM request WHERE uuid = ? AND type = ?''',
                     [uuid.lower(),type.lower()])
        row = c.fetchone()
        if row:
            requestid,requeststate = row[0],row[1]
        else:
            requeststate = 'pending'
            c.execute('''INSERT INTO request (id,uuid,state,type,ipv4,socketipv4,note)
                                     VALUES  (NULL,   ?,    ?,   ?,   ?,         ?,   ?)''',
                         [uuid.lower(),requeststate,type.lower(),
                          remoteclientipaddress,localclientaddress[0],note])
            fs.conn.commit()
            requestid = c.lastrowid

        #log this request as a polling
        c.execute('''INSERT INTO polling (timestamp,response,ipv4,socketipv4,uuid)
                                 VALUES  (        ?,       ?,   ?,         ?,   ?)''',
                     [time.asctime(),requeststate,remoteclientipaddress,localclientaddress[0],uuid.lower()])
        fs.conn.commit() 

        #return state as pending
        if requeststate in ('pending', 'revoked'):
            return (requeststate,
                    base64.b64encode("Flag approval is %s" % requeststate + NL))
        
        #if the flag is saved in the db, return it    
        c.execute('''SELECT content FROM flag WHERE requestid = ?''', [requestid])
        row = c.fetchone()
        if row:
            #flag already signed, return it
            return (requeststate, base64.b64encode(row[0]))
      
        #if the flag is not in the db, but it's approved
        #make the flag and save it 
        if requeststate == 'approved': 
            # query db, if request is approved, sign and save flag, return flag
            flagbody = "Request-received-from: %s:%d" % localclientaddress
            flagbody += NL
            flagbody += "Client-UUID: %s" % uuid
            flagbody += NL
            flagbody += "Remote-IP: %s" % remoteclientipaddress
            flagbody += NL
            flagbody += "Flag-type: %s" % type
            flagbody += NL + NL
            flagbody += note 
            signedflag = fs.makeflag(flagbody)

            c.execute('''INSERT INTO flag (requestid,content)
                                     VALUES (      ?,      ?)''',
                        [requestid,signedflag])
            c.execute('''UPDATE request SET state='signed' WHERE id = ?''',
                        [requestid])
            fs.conn.commit()

            return (requeststate, base64.b64encode(signedflag))
        
        # we shouldn't be here
        return ('error',base64.b64encode("Error in requestflagb64" + NL))

    def verifyflagb64(client_address, flag):
        return fs.verifyflag(base64.b64decode(flag))

    xmlrpcsrv.register_introspection_functions() # for .system.listMethods()
    xmlrpcsrv.register_function(requestflagb64)
    xmlrpcsrv.register_function(verifyflagb64)
    xmlrpcsrv.serve_forever()

if __name__ == '__main__':
    StartXMLRPCFlagServer()


