   function hideDiv(id,control)
   {
       document.getElementById(id).style.display = 'none';
       if(control != null) {
           cn = document.getElementById(control)
           cn.href="javascript:showDiv('" + id + "','" + control + "')"
           fliptextnode = document.createTextNode('Show')
           target = cn.firstChild
           cn.replaceChild(fliptextnode, target)
       }
   }
   function showDiv(id,control)
   {
       document.getElementById(id).style.display = 'block';
       if(control != null) {
           cn = document.getElementById(control)
           cn.href="javascript:hideDiv('" + id + "','" + control + "')"
           fliptextnode = document.createTextNode('Hide')
           target = cn.firstChild
           cn.replaceChild(fliptextnode, target)
       }
   }
