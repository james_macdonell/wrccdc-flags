import base64, hashlib
import os, os.path, sys
import M2Crypto.RSA, M2Crypto.BIO
import StringIO,textwrap

class FlagAuthority(object):
    def __init__(self,digestalgo='sha1',keysize=2048):
       self.digestalgo = digestalgo
       self.keysize = keysize
       self.rsakey = None  # a Crypto.PublicKey.RSA object
       self._rsainit()

    def _rsainit(self,keyfile= os.path.join('db', "flagkey.pem")):
        '''load rsa key from db; create a key if necessary'''
        if os.path.exists(keyfile):
           self.rsakey = M2Crypto.RSA.load_key(keyfile)
        else:
           self.rsakey = M2Crypto.RSA.gen_key(self.keysize,e=3) #e is generally 3
           self.rsakey.save_key(keyfile,cipher=None)
    
    def sign(self, plaintext):
       #plaintext="This is some plain text"
       #limited to sha1 or md5 by M2crypto interface for RSA.sign()
       m = hashlib.new(self.digestalgo)
       m.update(plaintext)
       sys.stderr.write("sign() using %s calculated %s\n" % (self.digestalgo, m.hexdigest()))
       digest = m.digest()
       signature = self.rsakey.sign(digest=digest,algo=self.digestalgo)
       #sys.stderr.write("sign() using %s calculated %s\n" % (self.digest, digest))
       return signature        

    def makeflag(self, textbody):
        signature = self.sign(textbody)
        b64sig = base64.b64encode(signature)
        delim = "----- %s -----\r\n"
        flag = delim % "BEGIN FLAG"
        flag += "\r\n"
        flag += textbody
        flag += "\r\n"
        flag += delim % "BEGIN RSA SIGNATURE"
        flag += "\r\n"
        flag += textwrap.fill(b64sig)
        flag += "\r\n"
        flag += delim % "END FLAG"
        return flag
 
        
    def parseflag(self, doc):
        state = "start"      
        textbody = ""
        b64sig = ""
        doc = StringIO.StringIO(doc)
        for line in doc.readlines():
            if state == "start" and line == "----- BEGIN FLAG -----\r\n":
               state = "flag_header"
               continue

            if state == "flag_header"  and line == "\r\n":
               state = "flag_body"
               continue

            if state == "flag_body"  and line == "----- BEGIN RSA SIGNATURE -----\r\n":
               state = "sig_header"
               textbody = textbody[:-2] #chomp extra \r\n
               continue

            if state == "sig_header"  and line == "\r\n":
               state = "sig_body"
               continue

            if state == "sig_body"  and line == "----- END FLAG -----":
               break

            if state == "flag_body":
               textbody += line

            if state == "sig_body":
               b64sig += line

        return textbody,b64sig

    def verifyflag(self, doc):
        textbody,b64sig = self.parseflag(doc)
        signature = base64.b64decode(b64sig)
        #sys.stderr.write('verify() using signature %s\n' % signature)
        m = hashlib.new(self.digestalgo)
        m.update(textbody)
        digest = m.digest()
        sys.stderr.write("verify() using %s calculated %s\n" % (self.digestalgo, m.hexdigest()))
        try:
           return self.rsakey.verify(digest,signature,algo=self.digestalgo)
        except M2Crypto.RSA.RSAError:
           return False

if __name__ == "__main__":
   fa = FlagAuthority()
   s = "This is some flag text.\r\nIt rocks!\r\n"
   flag = fa.makeflag(s)
   print flag
   if fa.verifyflag(flag):
       print "Flag verified"
   else:
       print "Flag not trusted"

   flag = fa.makeflag("This is some flag text.\r\n")
   for n in range(1,20):
       s =  ('%d\r' % n)*n
       flag = fa.makeflag(s)
       try:
           if fa.verifyflag(flag):
               print "Flag verified"
           else:
               print "Flag not trusted"
       except:
           print "Bad signature for '%s'\n" % s
   
