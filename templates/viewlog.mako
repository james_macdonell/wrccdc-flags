<%inherit file="base.mako" />
<%def name="title()">
Flag Logs WRCCDC
</%def>


<p>
View the raw log data of the flag server.

<h3 style="clear: both">Request Log</h3>

    <table>
    <thead>
    <tr>
    <th>Timestamp</th>
    <th>IP Outside</th>
    <th>IP Inside</th>
    <th>Response</th>
    <th>UUID</th>
    </tr>
    </thead>

    % for r in logrows: 
    <tr>
        % for key in 'timestamp','socketipv4','ipv4','response','uuid':
            % if key in r.keys():
                <td>${ r[key] | h }</td>
            % else:
                <td><i>None</i></td>
            % endif
        % endfor
    % endfor
    </table>
