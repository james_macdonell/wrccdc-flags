<%inherit file="base.mako" />
<%def name="title()">
Flag Admin WRCCDC
</%def>


<p>
Change the state of selected requests.

<h3 style="clear: both">Requests</h3>

<p>
<a href="viewlog">View Logs</a>

<form name="changestate" action="changestate" method="post">
    <table>
    <thead>
    <tr>
    <th>ID</th>
    <th>State</th>
    <th>Type</th>
    <th>IP Outside</th>
    <th>IP Inside</th>
    <th>Note</th>
    <th>UUID Conflits</th>
    </tr>
    </thead>

    % for r in requests: 
    <tr class="${r['state'] | h}">
        <td><input type="checkbox" name="request" value="${ r['id'] | h }"></td>
        % for key in 'state','type','ipv4','socketipv4':
            % if key in r.keys():
                <td>${ r[key] | h }</td>
            % else:
                <td><i>None</i></td>
            % endif
        % endfor
        <td>
        <div class="note"><pre>${ r['note'] | h}</pre></div>
        </td>
        <td>
        % if r['uuid'] in dups.keys():
           <span class="warn">${dups[r['uuid']]} &mdash; ${ r['uuid'] | h}</span>
        % else:
           -
        % endif
        </td>
    </tr>
    % endfor
    </table>
    <script>
        $("div.note").before('<a href="#" class="ctrl">Show</a>');
        $("div.note").hide();
        $("a.ctrl").toggle(function(){
            $(this).next().slideDown();
            $(this).text('Hide');
        }, function() {
            $(this).next().slideUp();
            $(this).text('Show');
        });
    </script>
    <input type="submit" name="action" value="Approve">
    <input type="submit" name="action" value="Revoke">
    </form>
    <p id="timer"></p>
<script>
   // some auto refresh
   var refreshIn = 30;
   $("p#timer").html("Refreshing in <span id=\"countdown\">" + refreshIn + "</span>... [<a id=\"timerctrl\" href=\"#\">Pause</a>]</p>")
   var timeoutId = setTimeout( "countdown()", 1*1000 );
   function countdown() {
       refreshIn = refreshIn - 1;
       $("span#countdown").text(refreshIn);
       timeoutId = setTimeout( "countdown()", 1*1000 );
       if (refreshIn == 0){
           window.location.reload(true);
       }
   }
   $("#timerctrl").toggle(function(){
       $(this).text("Resume");
       clearTimeout( timeoutId );
   }, function() {
       $(this).text("Pause");
       timeoutId = setTimeout( "countdown()", 1000 );
   });
</script>
