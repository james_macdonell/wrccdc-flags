import os
import cherrypy
from mako.template import Template
from mako.lookup import TemplateLookup
import sqlite3
import struct, socket

def connect(thread_index):
    db_file = os.path.join('db', 'flag.db')
    cherrypy.thread_data.db = sqlite3.connect(db_file)
    cherrypy.thread_data.db.row_factory = sqlite3.Row

class FlagAdmin(object):
    @staticmethod
    def _row_key(row):
        state_value = { 'error': 0, 'pending' : 1, 'approved': 2, 'signed': 3, 
                       'revoked': 4 }
        ip_out_val = struct.unpack('!I',socket.inet_aton(row['socketipv4']))
        ip_in_val = struct.unpack('!I',socket.inet_aton(row['ipv4']))
        return (state_value[row['state']],ip_out_val,ip_in_val)

    def __init__(self):
        self.mylookup = TemplateLookup(directories=['templates'])
        #self.edit = Edit()
        #self.delete = Delete()

    def index(self):
        c = cherrypy.thread_data.db.cursor()
        requests = c.execute('''SELECT id,uuid,state,type,ipv4,socketipv4,note
                                  FROM request''')
        sorted_requests = sorted(requests,key=FlagAdmin._row_key)

        dups = dict()
        dupuuids = c.execute('''SELECT uuid,COUNT(DISTINCT(socketipv4)) as c
                               FROM polling GROUP BY uuid HAVING c > 1''')
        for r in dupuuids:
            dups[r['uuid']] = r['c']

        templ = self.mylookup.get_template("index.mako")
        return templ.render(requests=sorted_requests,dups=dups)
    index.exposed = True

    def changestate(self,request,action):
        c = cherrypy.thread_data.db.cursor()
        referrer = cherrypy.request.headers['Referer']

        action_to_state = {'Approve': 'approved',
                           'Revoke':  'revoked'}

        if action not in action_to_state.keys():
            raise cherrypy.HTTPError(403, 'Invalid state change request')
        for id in request:
            c.execute('''UPDATE request SET state=? WHERE id=?''',
                         (action_to_state[action],id))
        cherrypy.thread_data.db.commit()
        raise cherrypy.HTTPRedirect(referrer)
    changestate.exposed = True

    def viewlog(self):
        c = cherrypy.thread_data.db.cursor()
        logs = c.execute('''SELECT timestamp, ipv4, socketipv4, response, uuid
                            FROM polling ORDER BY polling.ROWID LIMIT 300''')
        templ = self.mylookup.get_template("viewlog.mako")
        return templ.render(logrows=logs)
    viewlog.exposed = True


if __name__ == '__main__':

    # STATIC FILES
    current_dir = os.path.dirname(os.path.abspath(__file__))
    static_dir = os.path.join(current_dir, 'static')

    conf = {'/static': {'tools.staticdir.on': True,
                        'tools.staticdir.dir': static_dir,
                        'tools.staticdir.content_types': {'js': 'text/javascript',
                                                          'css': 'text/css',
                                                          'png': 'image/png'}
                       }
           }

    # DB HANDLING
    cherrypy.engine.subscribe('start_thread', connect)
    cherrypy.quickstart(FlagAdmin(),config=conf)
