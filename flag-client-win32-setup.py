from distutils.core import setup
import py2exe, sys

if sys.argv[-1:] != 'py2exe':
   sys.argv.append('py2exe')

setup(
    options = {'py2exe': {'bundle_files': 1}},
    console=['flag-client.py'],
    zipfile = None
)
