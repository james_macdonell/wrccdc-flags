import xmlrpclib, socket
import base64, uuid
import os, platform, sys, random, time
import subprocess
from subprocess import PIPE

def buildflags(COMPETITION = 'WRCCDC-2010-I',
               SERVER = 'localhost',
               PORT = 8000):

    #string on cmdline to an int, if necessary
    if isinstance(PORT, type("")):
        PORT = int(PORT)
        
    #request consists of
    # a uuid
    # local ip
    # a type: root, user, pii, etc
    # notes... we'll use netstat output

    #calculate filesystem root symbol, like c:\ or /
    if platform.system() == 'Windows':
        fsroot = os.path.expandvars('%SYSTEMDRIVE%') + "\\"  #C:\
    else:
        fsroot = '/'

    #read in the uuid, otherwise create it
    uuidpath = os.path.join(fsroot,'%s-UUID.txt' % COMPETITION)
    if os.path.exists(uuidpath):
       myuuid = open(uuidpath,'rb').read()
       myuuid = myuuid.strip()
    else:
       myuuid = str(uuid.uuid4()) #a random uuid
       fh = open(uuidpath, 'wb')
       fh.write(myuuid)
       fh.close()

    #get the client's perspective of what IP it posesses.
    #(since I can't get to the Socket object used by ServerProxy)
    sock = socket.socket()
    sock.connect((SERVER,PORT))
    clientip,clientport = sock.getsockname()
    sock.close()

    s = xmlrpclib.ServerProxy('http://%s:%d' % (SERVER,PORT))

    if platform.system() == 'Windows':
        netinfo = subprocess.Popen(["ipconfig", "/all"], stdout=PIPE).communicate()[0]
    else:
        netinfo = subprocess.Popen(["/sbin/ifconfig", "-a"], stdout=PIPE).communicate()[0]

    # root flag
    rootflagstatus, rootflag = s.requestflagb64(myuuid,clientip,'root',netinfo)
    rootflagpath = os.path.join(fsroot,'%s-ROOT-FLAG.txt' % COMPETITION)
    fh = open(rootflagpath, 'wb')
    fh.write(base64.b64decode(rootflag))
    fh.close()
    if platform.system() == 'Windows':
        #do some magic with cacls.exe
        sys.stderr.write("DEBUG: setting 'root' permissions on %s\n" % rootflagpath)
        p = subprocess.Popen(["cacls.exe", rootflagpath,
                                    "/P", "BUILTIN\\Administrators:F"],stdin=PIPE)
        p.stdin.write("Y") #Are you sure (Y/N)?
        p.stdin.close()
    else:
       os.chown(rootflagpath, 0,0) #root,root or root,wheel (etc)
       os.chmod(rootflagpath, 0600) #-rw-------


    # user flag
    userflagstatus, userflag = s.requestflagb64(myuuid,clientip,'user',netinfo)
    userflagpath = os.path.join(fsroot,'%s-USER-FLAG.txt' % COMPETITION)
    fh = open(userflagpath, 'wb')
    fh.write(base64.b64decode(userflag))
    fh.close()
    if platform.system() == 'Windows':
        sys.stderr.write("DEBUG: setting 'user' permissions on %s\n" % userflagpath)
        p = subprocess.Popen(["cacls.exe", userflagpath,
                              "/P", "BUILTIN\\Administrators:F",
                                    "NT AUTHORITY\\Authenticated Users:R"],stdin=PIPE)
        p.stdin.write("Y") #Are you sure (Y/N)?
        p.stdin.close()
    else:

       os.chmod(userflagpath, 0644) #-rw-r--r--

    #need to generate another uuid?
    if 'revoked' in (rootflagstatus,userflagstatus):
       os.unlink(uuidpath)

    #just checking

    if s.verifyflagb64(rootflag):
       #print base64.b64decode(flag)
       print "root flag OK"
    else:
       print "unverifiable root flag"

    if s.verifyflagb64(userflag):
       #print base64.b64decode(flag)
       print "user flag OK"
    else:
       print "unverifiable user flag"

if __name__ == '__main__':
    if len(sys.argv) <= 4: #proggie flagname server port
        delay = random.uniform(0.0, 10.0)
        sys.stderr.write("DEBUG: sleeping for %0.2f seconds.  Stand by...\n" % delay)
        time.sleep(delay)
        buildflags(*sys.argv[1:])
    else:
        print 'Usage %s ["FLAG-NAME-AND-Description" [flag-server [flag port]]]'
